#include "cs457_crypto.h"

int main(){

    char *message = NULL;
    uint8_t *plaintext, *ciphertext, *key;
    size_t size = 0;
    int N, i;

    printf("\033[0;31m");
    printf("\n# One time pad #\n");
    printf("\033[0m"); 

    printf("\nInsert message: ");
    size = getline(&message, &size, stdin);
    
    if(!valid_input(message)){
        printf("Invalid input!\n");
        return 0;
    }
    N = size-1;
    //printf("N: %d\n",N);
    key = (uint8_t*)malloc(sizeof(uint8_t)*N);
    key = generate_key(N);

    printf("\nkey: "); print_hex(key);

    plaintext = (uint8_t *)message;
    ciphertext = otp_encrypt(plaintext, key);
    printf("\nEncrypted: ");
    print_hex(ciphertext);

    memset(plaintext,0,size);

    plaintext = otp_decrypt(ciphertext, key);
    printf("\nDecrypted: %s\n", plaintext);

    if(message) free(message);
    if(plaintext) free(plaintext);
    if(ciphertext) free(ciphertext);
    if(key) free(key);

    return 0;
}