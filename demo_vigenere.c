#include "cs457_crypto.h"

int main(){

    char *message = NULL, *keyphrase;
    uint8_t *plaintext, *ciphertext, *key;
    size_t msg_size=0, key_size=0;

    printf("\033[0;31m");
    printf("\n# Vigenere's cipher #\n");
    printf("\033[0m");

    printf("\nInsert message: ");
    msg_size = getline(&message, &msg_size, stdin)-1;
    int i;
    for(i=0; i<msg_size; i++){
        if(!(UPPER_BOUND2<= message[i] && message[i]<=LOWER_BOUND2)){
            printf("Input characters must be uppercase letters!\n");
            return 0;
        }
    }
    printf("Insert keyphrase: ");
    key_size = getline(&keyphrase, &key_size, stdin)-1;
    for(i=0; i<key_size; i++){
        if(!(UPPER_BOUND2<= keyphrase[i] && keyphrase[i]<=LOWER_BOUND2)){
            printf("Input characters must be uppercase letters!\n");
            return 0;
        }
    }
    plaintext = (uint8_t*)malloc(sizeof(uint8_t)*msg_size);
    plaintext = (uint8_t *)message;

    /*printf("%zu %zu\n",msg_size,key_size);*/

    key = expand_keyphrase(plaintext, keyphrase);
    printf("key: %s\n",key);

    ciphertext = vigenere_encrypt(plaintext, key);
    printf("\nEncrypted: %s\n", ciphertext);

    memset(plaintext,0,msg_size);

    plaintext = vigenere_decrypt(ciphertext, key);
    printf("\nDecrypted: %s\n", plaintext);

    if(message) free(message);
    if(keyphrase) free(keyphrase);
    if(plaintext) free(plaintext);
    if(ciphertext) free(ciphertext);

    return 0;
}