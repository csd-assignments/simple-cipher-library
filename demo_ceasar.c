#include "cs457_crypto.h"

int main(){

    char *message = NULL;
    uint8_t *plaintext, *ciphertext; // uint8_t = 8 bits = 1 char
    size_t size = 0;
    unsigned short N;

    printf("\033[0;31m");
    printf("\n# Ceasar's cipher #\n");
    printf("\033[0m"); 
    printf("\nInsert message: ");

    size = getline(&message, &size, stdin);
    if(!valid_input(message)){
        printf("Invalid input!\n");
        return 0;
    }
    printf("Insert shift N: ");
    scanf("%hu", &N);

    plaintext = (uint8_t*)malloc(sizeof(uint8_t)*size);
    plaintext = (uint8_t *)message;

    /*printf("\nmessage: %s\n", message);
    printf("size: %zu\n", strlen(plaintext));*/

    ciphertext = caesar_encrypt(plaintext, N);
    printf("\nEncrypted: %s\n", ciphertext);

    memset(plaintext,0,size);

    plaintext = caesar_decrypt(ciphertext, N);
    printf("\nDecrypted: %s\n", plaintext);

    if(message) free(message);
    if(plaintext) free(plaintext);
    if(ciphertext) free(ciphertext);

    return 0;
}