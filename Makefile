all: ceasar spartan otp vigenere

ceasar: demo_ceasar.c cs457_crypto.c
	gcc -o ceasar demo_ceasar.c cs457_crypto.c

spartan: demo_spartan.c cs457_crypto.c
	gcc -o spartan demo_spartan.c cs457_crypto.c

otp: demo_otp.c cs457_crypto.c
	gcc -o otp demo_otp.c cs457_crypto.c

vigenere: demo_vigenere.c cs457_crypto.c
	gcc -o vigenere demo_vigenere.c cs457_crypto.c

clean:
	rm ceasar spartan otp vigenere