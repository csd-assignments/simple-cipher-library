#include "cs457_crypto.h"

uint8_t * caesar_encrypt(uint8_t *plaintext, unsigned short N){

    uint8_t *ciphertext;
    uint8_t c, position;
    int i, size = strlen(plaintext);
    int legal_steps;

    ciphertext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i = 0; i < size; i++) {

        position = plaintext[i];
        /*printf("position: %d\n",position);*/

        if(position == 10){ /*new line*/
            ciphertext[i] = 0;
            break;
        }
        if(position>=UPPER_BOUND1 && position<=LOWER_BOUND1){ /*0-9*/

            legal_steps = LOWER_BOUND1 - position;
            if(N > legal_steps){
                c = UPPER_BOUND2 + (N-legal_steps)-1;
            }else{
                c = position+N;
            }
        }
        else if(position>=UPPER_BOUND2 && position<=LOWER_BOUND2){/*A-Z*/

            legal_steps = LOWER_BOUND2 - position;
            if(N > legal_steps){
                c = UPPER_BOUND3 + (N-legal_steps)-1;
            }else{
                c = position+N;
            }
        }
        else if(position>=UPPER_BOUND3 && position<=LOWER_BOUND3){/*a-z*/

            legal_steps = LOWER_BOUND3 - position;
            if(N > legal_steps){
                c = UPPER_BOUND1 + (N-legal_steps)-1;
            }else{
                c = position+N;
            }
        }
        ciphertext[i] = c;
    }

    return ciphertext;
}

uint8_t * caesar_decrypt(uint8_t *ciphertext, unsigned short N){

    uint8_t *plaintext;
    uint8_t c, position;
    int i, size = strlen(ciphertext);
    int legal_steps;

    plaintext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i = 0; i < size; i++) {

        position = ciphertext[i];

        if(position>=UPPER_BOUND1 && position<=LOWER_BOUND1){ /*0-9*/

            legal_steps = position - UPPER_BOUND1;
            if(N > legal_steps){
                c = LOWER_BOUND3 - (N-legal_steps)+1;
            }else{
                c = position-N;
            }
        }
        else if(position>=UPPER_BOUND2 && position<=LOWER_BOUND2){/*A-Z*/
    
            legal_steps = position - UPPER_BOUND2;
            if(N > legal_steps){
                c = LOWER_BOUND1 - (N-legal_steps)+1;
            }else{
                c = position-N;
            }
        }
        else if(position>=UPPER_BOUND3 && position<=LOWER_BOUND3){/*a-z*/

            legal_steps = position - UPPER_BOUND3;
            if(N > legal_steps){
                c = LOWER_BOUND2 - (N-legal_steps)+1;
            }else{
                c = position-N;
            }
        }
        plaintext[i] = c;
    }

    return plaintext;
}

uint8_t * spartan_encrypt(uint8_t *plaintext, unsigned short circ, unsigned short len){

    uint8_t *ciphertext;
    int i,j,size = strlen(plaintext);
    uint8_t skytale[circ][len];
    uint8_t k = 0;

    if(size > circ*len){
        printf("\nMessage is longer than circ*len!\n");
        return NULL;
    }

    ciphertext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i=0; i<circ; i++){
        for(j=0; j<len; j++){
            if(k >= size-1) {
                skytale[i][j] = 35; //#
                k++; continue;
            }
            skytale[i][j] = plaintext[k++];
        }
    }
    /*for(i=0; i<circ; i++){
        for(j=0; j<len; j++){
            printf("%c",skytale[i][j]);
        }
        printf("\n");
    }*/
    k = 0;
    for(j=0; j<len; j++){
        for(i=0; i<circ; i++){
            if(skytale[i][j]==35) break;
            ciphertext[k++] = skytale[i][j];
        }
    }
    return ciphertext;
}

uint8_t * spartan_decrypt(uint8_t *ciphertext, unsigned short circ, unsigned short len){

    uint8_t *plaintext;
    int i,j,size = strlen(ciphertext);
    uint8_t skytale[circ][len];
    uint8_t k = 0;

    //printf("size: %d\n",size);
    int padding = circ*len - size;
    //printf("padding: %d\n",padding);

    /*add padding at the end of the array*/
    for(i=circ-1; i>=0; i--){
        for(j=len-1; j>=0; j--){
            if(padding==0) break;
            skytale[i][j] = 35;
            padding--;
        }
    }

    plaintext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(j=0; j<len; j++){
        for(i=0; i<circ; i++){
            if(skytale[i][j] == 35) {continue;}
            skytale[i][j] = ciphertext[k++];
        }
    }
    for(i=0; i<circ; i++){
        for(j=0; j<len; j++){
            printf("%c",skytale[i][j]);
        }
        printf("\n");
    }
    k = 0;
    for(i=0; i<circ; i++){
        for(j=0; j<len; j++){
            if(skytale[i][j]==35) break;
            plaintext[k++] = skytale[i][j];
        }
    }
    return plaintext;
}

int valid_input(char *message){
    int i, size, valid = 1;

    size = strlen(message);

    for(i=0; i<size-1; i++){
        if((UPPER_BOUND1<= message[i] && message[i]<=LOWER_BOUND1)
        || (UPPER_BOUND2<= message[i] && message[i]<=LOWER_BOUND2)
        || (UPPER_BOUND3<= message[i] && message[i]<=LOWER_BOUND3)
        )  valid = 1;
        else {
            //printf("invalid\n");
            valid = 0; 
            break;
        }
    }
    return valid;
}

void print_hex(uint8_t *array){
    int i, size = strlen(array);
    for(i = 0; i < size; ++i)
        printf("%02X ", array[i]);
    printf("\n");
}

uint8_t *generate_key(int N){
    
    int fd;
    uint8_t *key = (uint8_t*)malloc(sizeof(uint8_t)*N);
    
    fd = open("/dev/urandom", O_RDONLY);
    read(fd, key, N);
    /*key now contains the random data*/
    close(fd);
    return key;
}

uint8_t * otp_encrypt(uint8_t *plaintext, uint8_t *key){

    uint8_t *ciphertext;
    int i,size = strlen(plaintext);

    ciphertext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i=0; i<size-1; i++){
        ciphertext[i] = plaintext[i]^key[i];
    }
    return ciphertext;
}

uint8_t * otp_decrypt(uint8_t *ciphertext, uint8_t *key){

    uint8_t *plaintext;
    int i,size = strlen(ciphertext);

    plaintext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i=0; i<size; i++){
        plaintext[i] = ciphertext[i]^key[i];
    }
    return plaintext;
}

uint8_t *expand_keyphrase(uint8_t *plaintext, char *keyphrase){

    int i, k = 0, size = strlen(plaintext)-1;
    //printf("size %d",size);
    uint8_t *key = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i=0; i<size; i++){
        if(keyphrase[k] == 10) {i--; k++; continue;} /*new line*/
        if(k == strlen(keyphrase)){
            k = 0;
        }
        key[i] = keyphrase[k++];
    }
    return key;
}

uint8_t * vigenere_encrypt(uint8_t *plaintext, uint8_t *key){

    uint8_t *ciphertext;
    uint8_t letter;
    int i,size = strlen(plaintext);

    ciphertext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i=0; i<size-1; i++){
        letter = plaintext[i] + (key[i]-65);
        if(letter > LOWER_BOUND2){ /*Z*/
            letter = UPPER_BOUND2 /*A*/ + (letter-LOWER_BOUND2-1);
        }
        ciphertext[i] = letter;
    }
    return ciphertext;
}

uint8_t * vigenere_decrypt(uint8_t *ciphertext, uint8_t *key){

    uint8_t *plaintext;
    uint8_t letter;
    int i,size = strlen(ciphertext);

    plaintext = (uint8_t*)malloc(sizeof(uint8_t)*size);

    for(i=0; i<size; i++){
        letter = ciphertext[i] - (key[i]-65);
        if(letter < UPPER_BOUND2){
            //printf("%d ",i);
            letter = LOWER_BOUND2 - (UPPER_BOUND2-letter-1);
        }
        plaintext[i] = letter;
    }
    return plaintext;
}