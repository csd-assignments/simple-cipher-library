#include "cs457_crypto.h"

int main(){

    char *message = NULL;
    uint8_t *plaintext, *ciphertext;
    size_t size = 0;
    unsigned short circ, len;

    printf("\033[0;31m");
    printf("\n# Spartan's cipher #\n");
    printf("\033[0m"); 

    printf("\nInsert message: ");
    size = getline(&message, &size, stdin);
    if(!valid_input(message)){
        printf("Invalid input!\n");
        return 0;
    }
    printf("Insert circ and len: ");
    scanf("%hu %hu", &circ, &len);

    plaintext = (uint8_t *)message;

    ciphertext = spartan_encrypt(plaintext, circ, len);

    memset(plaintext,0,size);
    if(ciphertext){
        printf("\nEncrypted: %s\n", ciphertext);
        plaintext = spartan_decrypt(ciphertext, circ, len);
        printf("\nDecrypted: %s\n", plaintext);
    }

    if(message) free(message);
    if(plaintext) free(plaintext);
    if(ciphertext) free(ciphertext);

    return 0;
}