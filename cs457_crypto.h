#ifndef _CS457_CRYPTO_H_
#define _CS457_CRYPTO_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#define UPPER_BOUND1 48
#define LOWER_BOUND1 57

#define UPPER_BOUND2 65
#define LOWER_BOUND2 90

#define UPPER_BOUND3 97
#define LOWER_BOUND3 122

int valid_input(char *message);

uint8_t * caesar_encrypt(uint8_t *plaintext, unsigned short N);
uint8_t * caesar_decrypt(uint8_t *ciphertext, unsigned short N);

uint8_t * spartan_encrypt(uint8_t *plaintext, unsigned short circ, unsigned short len);
uint8_t * spartan_decrypt(uint8_t *ciphertext, unsigned short circ, unsigned short len);

void print_hex(uint8_t *array);

uint8_t * generate_key(int N);

uint8_t * otp_encrypt(uint8_t *plaintext, uint8_t *key);
uint8_t * otp_decrypt(uint8_t *ciphertext, uint8_t *key);

uint8_t * expand_keyphrase(uint8_t *plaintext, char *keyphrase);

uint8_t * vigenere_encrypt(uint8_t *plaintext, uint8_t *key);
uint8_t * vigenere_decrypt(uint8_t *ciphertext, uint8_t *key);

#endif /* _CS457_CRYPTO_H_ */